# Template for backend course
I made a telegram bot that can collect information from
a person and pass it to the web application model. 
Data exchange between the web application and the telegram bot is 
implemented using the REST API.

https://www.loom.com/share/1edfbcad8e8c487fb784d1b7167e8d15?sharedAppSource=personal_library

By clicking on this link you will see a demonstration of the web application, telegram bot and Rest API work.

# To launch project, you need to: 

1. Install venv and requirements
2. Launch Bot
3. Launch Django web server

# Install venv and requirements

1. Activate virtual environment
2. From root folder run `pip install -r requirements.txt`

# Launch bot:
1. Migrate. ```python manage.py migrate```
2. Obtain telegram bot api token
3. Make `local_settings.py` file, and paste your API_TOKEN here:
   ```(sh)
   cp src/app/internal/transport/bot/bot_settings.example.py src/app/internal/transport/bot/bot_settings.py
   nano src/app/internal/transport/bot/bot_settings.py # paste bot token
   ```   
4. Simply launch `python src/app/internal/transport/bot/handlers.py` from root directory of the project
5. Open your bot in telegram and write `/start`

# Launch django app:

1. run `python src/manage.py runserver`
2. navigate to `http://127.0.0.1:8000/`
