from rest_framework import viewsets

from .serializers import PersonSerializer
from .models import Person


class PersonViewSet(viewsets.ModelViewSet):
    queryset = Person.objects.all()
    serializer_class = PersonSerializer


class MyPersonViewSet(viewsets.ModelViewSet):
    serializer_class = PersonSerializer

    def get_queryset(self):
        queryset = Person.objects.all()
        telegram_user_id = self.request.query_params.get('telegram_user_id')
        queryset = queryset.filter(telegram_user_id=telegram_user_id)
        return queryset
