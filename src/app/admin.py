from django.contrib import admin
from .models import Person

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"


class PersonAdmin(admin.ModelAdmin):
    model = Person
    list_display = ['full_name', 'email', 'age', 'phone_number']
    exclude = ['telegram_user_id']


admin.site.register(Person, PersonAdmin)
