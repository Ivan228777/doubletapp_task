from django.db import models


class Person(models.Model):
    full_name = models.CharField(max_length=2048)
    email = models.CharField(max_length=2048)
    age = models.CharField(max_length=2048)
    phone_number = models.CharField(max_length=2048)
    telegram_user_id = models.CharField(max_length=2048, blank=True, null=True)