import telebot
import requests

from bot_settings import API_TOKEN

bot = telebot.TeleBot(API_TOKEN)

full_name = ''
age = 0
email = ''
phone_number = 0
arr = []
telegram_user_id = ''
BASE_URL = "http://127.0.0.1:8000"


@bot.message_handler(content_types=['text'])
def first_message(message):
    if message.text != '/start':
        bot.send_message(message.chat.id, 'Для начала работы бота напишите /start')
    else:
        bot.send_message(message.chat.id, 'Добро пожаловать. Я готов занести ваши данные в БД! Напишите /set_phone')
        bot.register_next_step_handler(message, start_message)


@bot.message_handler(content_types=['text'])
def start_message(message):
    if message.text != '/set_phone':
        bot.send_message(message.chat.id, 'Напишите /set_phone')
        bot.register_next_step_handler(message, start_message)
    else:
        bot.send_message(message.chat.id, 'Введите свой номер телефона')
        bot.register_next_step_handler(message, get_phone)


@bot.message_handler(content_types=['text'])
def get_phone(message):
    global phone_number
    phone_number = message.text
    if phone_number.isdigit() == False or phone_number[0] == '/':
        bot.send_message(message.chat.id, 'Некорректный ввод. Пожалуйста повторите попытку')
        bot.register_next_step_handler(message, get_phone)
    else:
        arr.append(phone_number)
        bot.send_message(message.chat.id, 'Введите ваше имя и фамилию')
        bot.register_next_step_handler(message, get_full_name)


@bot.message_handler(content_types=['text'])
def get_full_name(message):
    global full_name
    full_name = message.text
    if full_name.isdigit() == True or full_name[0] == '/':
        bot.send_message(message.chat.id, 'Некорректный ввод. Пожалуйста повторите попытку')
        bot.register_next_step_handler(message, get_full_name)
    else:
        arr.append(full_name)
        bot.send_message(message.chat.id, 'Введите свой возраст')
        bot.register_next_step_handler(message, get_age)


@bot.message_handler(content_types=['text'])
def get_age(message):
    global age
    age = message.text
    if age.isdigit() == False or age[0] == '/':
        bot.send_message(message.chat.id, 'Некорректный ввод. Пожалуйста повторите попытку')
        bot.register_next_step_handler(message, get_age)
    else:
        arr.append(age)
        bot.send_message(message.chat.id, 'Введите адрес своей электронной почты')
        bot.register_next_step_handler(message, get_email)


@bot.message_handler(content_types=['text'])
def get_email(message):
    global email
    email = message.text
    if email.isdigit() == True or email[0] == '/':
        bot.send_message(message.chat.id, 'Некорректный ввод. Пожалуйста повторите попытку')
        bot.register_next_step_handler(message, get_email)
    else:
        arr.append(email)
        global telegram_user_id
        telegram_user_id = message.from_user.id
        telegram_user_id = str(telegram_user_id)
        arr.append(telegram_user_id)
        requests.post(f'{BASE_URL}/api/Persons/',
                      json={'telegram_user_id': arr[4], 'full_name': arr[1], 'age': arr[2], 'email': arr[3],
                            'phone_number': arr[0]})
        bot.send_message(message.chat.id,
                         'Ваши данные успешно занесены в базу! Вы можете посмотреть занесенную информацию написав /me или завершить работу бота командой /exit')
        bot.register_next_step_handler(message, show_user_data)


@bot.message_handler(content_types=['text'])
def show_user_data(message):
    if message.text == '/me':
        bot.send_message(message.chat.id,
                         '| Ваш номер телефона: ' + arr[0] + '| Ваше имя: ' + arr[1] + '| Ваш возраст: ' + arr[
                             2] + '| Ваш Email: ' + arr[3] + " |")
        bot.send_message(message.chat.id,
                         'По этой ссылке вы сможете увидеть введенные данные через Http запрос: 'f'{BASE_URL}/api/me/?telegram_user_id='f'{telegram_user_id}')
        bot.send_message(message.chat.id, 'Работа бота завершена')
        bot.stop_polling()
    elif message.text == '/exit':
        bot.send_message(message.chat.id, 'Работа бота завершена')
        bot.stop_polling()
    else:
        bot.send_message(message.chat.id, 'Некорректный ввод. Пожалуйста повторите попытку')
        bot.register_next_step_handler(message, show_user_data)


bot.polling(none_stop=True)
