from django.urls import include, path
from rest_framework import routers
import sys

sys.path.insert(0, '..')
from .. import views

router = routers.DefaultRouter()
router.register(r'Persons', views.PersonViewSet, basename="PersonView")
router.register(r'me', views.MyPersonViewSet, basename="MePersonView")

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
