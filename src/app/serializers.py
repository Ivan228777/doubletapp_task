from rest_framework import serializers

from .models import Person


class PersonSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Person
        fields = ('id', 'telegram_user_id', 'full_name', 'email', 'age', 'phone_number')
